// inicializando google maps en la direccion deseada
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.448513, lng: -70.667273},
    zoom: 18,
  },);

  // posicion del marcadorcito
  var marker = new google.maps.Marker({
    position: {lat: -33.448956, lng: -70.667231},
    icon: 'img/marker-map.png',
    map: map
  });

  // despues de 5 segundos vuelve al centro del marcador
  // cuando cambia la posicion del mapa
  map.addListener('center_changed', function() {
    window.setTimeout(function() {
      map.panTo({lat: -33.448513, lng: -70.667273});
    }, 5000);
  });

  // globo de texto con la direccion
  var mapSticky = 
  '<div class="map-globe p-3">'+
    '<b class="kelsonBold">Datacity IoT</b>'+
    '<p>'+
      'PJE. REP&Uacute;BLICA 54, SANTIAGO, '+
      '<br>'+
      'REGI&Oacute;N METROPOLITANA'+
    '</p>'+
    '<div class="map-social">'+
      '<a class="twitter" href="#">'+
        '<i class="icon ion-logo-twitter"></i>'+
      '</a>'+
      '<a class="facebook" href="#">'+
        '<i class="icon ion-logo-facebook"></i>'+
      '</a>'+
      '<a href="#">'+
        '<img src="img/icono-car.png" class="pb-2">'+
      '</a>'+
    '</div>'+
  '</div>';

  // crea un objeto infoWindow con el contenido 
  // del globo de texto en mapSticky
  var infowindow = new google.maps.InfoWindow({
    content: mapSticky
  });

  // agrega el globo al marcador
  function attachMessage(marker, message) {
    marker.addListener('click', function() {
      infowindow.open(marker.get('map'), marker);
    });
  }
  
  // llama a la funcion para mostrar el globo 
  // al hacer click en el marcador
  attachMessage(marker, mapSticky);

  // carga el mensaje por default
  google.maps.event.addListenerOnce(map, 'tilesloaded', function() {
    infowindow.open(map, marker);
  });
}